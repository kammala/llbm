#!/usr/bin/env python3
import os	
import csv
import argparse
import sqlite3
import datetime

db = sqlite3.connect('./boxes.db')
DEFAULT_DAYS_COUNT = 0
EMPTY_INPUTS = {'', '\n', os.linesep}

def create_table():
	cursor = db.cursor()
	cursor.execute('''
		create table translations (
			id integer primary key autoincrement, 
			word varchar(256) not null unique,
			translation text,
			next_appearance datetime not null
		)
	''')
	db.commit()
	cursor.close()

def reset():
	cursor = db.cursor()
	cursor.execute('''
		update translations set next_appearance = CURRENT_TIMESTAMP
	''')
	db.commit()
	cursor.close()

def add(filename=None):
	if filename is None:
		words_iter = get_words_with_translation()
	else:
		words_iter = get_words_from_file(filename)
	cursor = db.cursor()
	for word, translation in words_iter:
		cursor.execute('select id, translation from translations where word=?', [word])
		existed_word = cursor.fetchone()
		if existed_word is not None:
			wid = existed_word[0]
			old_translation = existed_word[1]
			print('Warning! Updating "{}": "{}" to "{}"'.format(wid, old_translation, translation))
			cursor.execute('update translations set translation = ? where id = ?', [translation, wid])
		else:
			cursor.execute('insert into translations (word, translation, next_appearance) values (?, ?, CURRENT_TIMESTAMP)', [word, translation])
		db.commit()
	cursor.close()


def get_words_with_translation():
	while True:
		print('Input word and translation to it. Type Ctrl-D to stop.')
		word, trans = get_word_from_input()
		if word:
			yield word, trans
		else:
			break

def get_word_from_input():
	word = trans = None
	try:
		word = input('Input word:')
		word = word if word not in EMPTY_INPUTS else None
		if word:
			trans = input('Input translation:')
			trans = trans if trans not in EMPTY_INPUTS else None
	except EOFError:
		pass
	return word, trans

def get_words_from_file(filename):
	with open(filename) as fl:
		d = csv.Sniffer().sniff(fl.read(1024))
		fl.seek(0)
		rdr = csv.DictReader(fl, dialect=d)
		for row in rdr:
			yield row['word'], row['translation']

def get_today_list():
	cursor = db.cursor()
	cursor.execute('''
		select * from translations where next_appearance < CURRENT_TIMESTAMP order by random()
	''')
	cols = [x[0] for x in cursor.description]
	data = [dict(zip(cols, row)) for row in cursor.fetchall()]
	cursor.close()
	return data

def print_data_box(data, sym=':'):
	maxlen = max(0, *map(len, data.split('\n')))
	left_ear = sym*2 + ' '*2
	rght_ear = ' '*2 + sym*2
	print(sym * (maxlen + len(left_ear) + len(rght_ear)))
	print(left_ear, ' ' * maxlen, rght_ear, sep='')
	for row in data.split('\n'):
		print(left_ear, row.ljust(maxlen), rght_ear, sep='')
	print(left_ear, ' ' * maxlen, rght_ear, sep='')
	print(sym * (maxlen + len(left_ear) + len(rght_ear)))

def check_is_end():
	while True:
		try:
			inp = input('Type "q" or Ctrl-D to quit, "c" to continue:').lower()
			if inp == "q":
				return True
			if inp == "c":
				return False 
		except EOFError:
			return True
		print('Incorrect input: "{}"'.format(inp))

def get_days_count():
	while True:
		print('Choose box variant or input days count to next appearance of last word')
		print('Default is {} days'.format(DEFAULT_DAYS_COUNT))
		print('To quit press Ctrl-D')
		print('Boxes:')
		print('a) today')
		print('b) tomorrow')
		print('c) 3 days later')
		print('d) 1 week later')
		print('e) 1 month later')
		inp = input('Your choice:').lower()			
		days_variants = {'a': 0, 'b': 1, 'c': 3, 'd': 7, 'e': 30, 'd': 1, 'w': 7, 'm': 30}
		days = days_variants.get(inp)
		if inp in EMPTY_INPUTS:
			days = DEFAULT_DAYS_COUNT
		if days is None:
			try:
				days = int(inp)
			except ValueError:
				print('Incorrect input: "{}"'.format(inp))
				continue
		return days

def run_once():
	for word in get_today_list():
		process_word(word)
	else:
		print('There is nothing left to learn!')

def process_word(word):
	print('Try to remember translation for:')
	print_data_box(word['word'].upper())
	enter = input('Press Enter to view translation')
	print('Translation:')
	print_data_box(word['translation'], sym='!')
	days = get_days_count()
	update_word_appearance(word['id'], days)

def update_word_appearance(word_id, days):
	cursor = db.cursor()
	next_appearance = datetime.datetime.now() + datetime.timedelta(days=days)
	cursor.execute("update translations set next_appearance=? where id = ?", [next_appearance, word_id])
	cursor.close()
	db.commit()

def endless_loop():
	try:
		is_end = False
		while not is_end:
			run_once()
			is_end = check_is_end()
	except (KeyboardInterrupt, EOFError):
		print('Goodbye!')
	
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.set_defaults(func=endless_loop)
	sp = parser.add_subparsers()
	p = sp.add_parser('run')
	p = sp.add_parser('createdb')
	p.set_defaults(func=create_table)
	p = sp.add_parser('reset')
	p.set_defaults(func=reset)
	p = sp.add_parser('add')
	p.add_argument('--csv', dest='filename')
	p.set_defaults(func=add)

	args = vars(parser.parse_args())
	func = args.pop('func')
	func(**args)

